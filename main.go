package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"net/http"
)

var (
	DB *gorm.DB
)

//type Todo struct {
//	ID     uint
//	Age    uint
//	Name   string
//	Gender string
//	Hobby  string
//}

type Todo struct {
	ID     int    `json:"id"`
	Title  string `json:"title"`
	Status bool   `json:"status"`
}

func initmysql() (err error) {
	DB, err = gorm.Open("mysql", "root:123456@(localhost)/db_demo001?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		fmt.Printf("connect msyql failed, err: %v \n", err)
		return
	}
	// 测试是否能够连通
	return DB.DB().Ping()
}

func main() {

	err := initmysql()
	if err != nil {
		panic(err)
	} else {
		fmt.Println("connect mysql success")
	}
	// 延迟关闭数据库
	defer DB.Close()

	// 模型关闭 自动迁移【把结构体和数据表进行对应】
	DB.AutoMigrate(&Todo{})

	r := gin.Default()
	r.Static("/static", "static")
	r.LoadHTMLGlob("templates/*")
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	v1Group := r.Group("v1")
	{
		// 添加
		v1Group.POST("/todo", func(context *gin.Context) {
			// 从请求中把数据取出来
			var todo Todo
			context.BindJSON(&todo)
			// 存入数据库
			err := DB.Create(&todo).Error
			// 响应
			if err != nil {
				context.JSON(http.StatusOK, gin.H{
					"error": err.Error(),
				})
			} else {
				context.JSON(http.StatusOK, todo)
			}
		})
		// 查看【查看所有】
		v1Group.GET("/todo", func(context *gin.Context) {
			// 查询所有数据
			var todoList []Todo
			err := DB.Find(&todoList).Error
			if err != nil {
				context.JSON(http.StatusOK, gin.H{
					"error": err.Error(),
				})
			} else {
				context.JSON(http.StatusOK, todoList)
			}
		})
		// 查看【查看某个】
		v1Group.GET("/todo/:id", func(context *gin.Context) {

		})
		// 修改
		v1Group.PUT("/todo/:id", func(context *gin.Context) {
			id, ok := context.Params.Get("id")
			if !ok {
				context.JSON(http.StatusOK, gin.H{
					"error": err.Error(),
				})
			} else {
				var todo Todo
				err := DB.Where("id=?", id).First(&todo).Error
				if err != nil {
					context.JSON(http.StatusOK, gin.H{
						"error": err.Error(),
					})
				} else {
					// 将内容进行修改
					context.BindJSON(&todo)
					err = DB.Save(&todo).Error
					if err != nil {
						context.JSON(http.StatusOK, gin.H{
							"error": err.Error(),
						})
					} else {
						context.JSON(http.StatusOK, todo)
					}
				}
			}
		})

		// 删除
		v1Group.DELETE("/todo/:id", func(context *gin.Context) {
			id, ok := context.Params.Get("id")
			if !ok {
				context.JSON(http.StatusOK, gin.H{
					"error": err.Error(),
				})
			} else {
				var todo Todo
				err := DB.Where("id=?", id).Delete(todo)
				if err != nil {
					context.JSON(http.StatusOK, gin.H{
						"error": err,
					})
				} else {
					context.JSON(http.StatusOK, todo)
				}
			}
		})
	}

	r.Run(":12345")
}
